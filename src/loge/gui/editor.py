# -*- coding: utf-8 -*-
#-----------------------------------------------------------------
# Copyright (C) 2020, the Loge development team
#
# This file is part of Loge
# Loge is distributed under the terms of GNU General Public License
# The full license can be found in 'license.txt'
# Loge development team can be found in 'development.txt'
#-----------------------------------------------------------------

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import loge.gui.syntax as syntax
from  loge.gui.greek_dict import greek_dict
 
class LineNumberArea(QWidget):
 
    def __init__(self, editor):
        super().__init__(editor)
        self.myeditor = editor
 
    def sizeHint(self):
        return Qsize(self.editor.lineNumberAreaWidth(), 0)
 
    def paintEvent(self, event):
        self.myeditor.lineNumberAreaPaintEvent(event)

class CodeEditor(QPlainTextEdit):
    def __init__(self, parent = None):
        super(CodeEditor,self).__init__(parent)
        self.highlight = syntax.PythonHighlighter(self.document())
        
        self.lineNumberArea = LineNumberArea(self)
 
        self.blockCountChanged.connect(self.updateLineNumberAreaWidth)
        self.updateRequest.connect(self.updateLineNumberArea)
        self.cursorPositionChanged.connect(self.highlightCurrentLine)
        
        self.updateLineNumberAreaWidth(0)
        
        self.setTabStopWidth(self.fontMetrics().width(' ') * 4)
        self.setStyleSheet("""QPlainTextEdit {background-color: white;
                            font-family: Courier New;}""")
        self.zoomIn(2)   
 
    def lineNumberAreaWidth(self):
        digits = 1
        count = max(1, self.blockCount())
        while count >= 10:
            count /= 10
            digits += 1
        space = 3 + self.fontMetrics().width('9') * digits
        return space
 
 
    def updateLineNumberAreaWidth(self, _):
        self.setViewportMargins(self.lineNumberAreaWidth(), 0, 0, 0)
 
 
    def updateLineNumberArea(self, rect, dy):
 
        if dy:
            self.lineNumberArea.scroll(0, dy)
        else:
            self.lineNumberArea.update(0, rect.y(), self.lineNumberArea.width(),
                       rect.height())
 
        if rect.contains(self.viewport().rect()):
            self.updateLineNumberAreaWidth(0)
 
 
    def resizeEvent(self, event):
        super().resizeEvent(event)
 
        cr = self.contentsRect();
        self.lineNumberArea.setGeometry(QRect(cr.left(), cr.top(),
                    self.lineNumberAreaWidth(), cr.height()))
        self.setTabStopWidth(self.fontMetrics().width(' ') * 4)
 
 
    def lineNumberAreaPaintEvent(self, event):
        mypainter = QPainter(self.lineNumberArea)
 
        mypainter.fillRect(event.rect(), Qt.lightGray)
 
        block = self.firstVisibleBlock()
        blockNumber = block.blockNumber()
        top = self.blockBoundingGeometry(block).translated(self.contentOffset()).top()
        bottom = top + self.blockBoundingRect(block).height()
 
        # Just to make sure I use the right font
        height = self.fontMetrics().height()
        while block.isValid() and (top <= event.rect().bottom()):
            if block.isVisible() and (bottom >= event.rect().top()):
                number = str(blockNumber + 1)
                mypainter.setPen(Qt.black)
                mypainter.drawText(0, int(top), self.lineNumberArea.width(), height,
                 Qt.AlignRight, number)
 
            block = block.next()
            top = bottom
            bottom = top + self.blockBoundingRect(block).height()
            blockNumber += 1
 
    def highlightCurrentLine(self):
        extraSelections = []
 
        if not self.isReadOnly():
            selection = QTextEdit.ExtraSelection()
 
            lineColor = QColor(Qt.lightGray).lighter(120)
 
            selection.format.setBackground(lineColor)
            selection.format.setProperty(QTextFormat.FullWidthSelection, True)
            selection.cursor = self.textCursor()
            selection.cursor.clearSelection()
            extraSelections.append(selection)
        self.setExtraSelections(extraSelections)

    def get_scroll_relposition(self):
        absposition = self.verticalScrollBar().value()
        absmaximum = self.verticalScrollBar().maximum()
        if absmaximum != 0:
            relposition = 1.0 * absposition / absmaximum
        else:
            relposition = 0.0
        return relposition
    
    def insert_greek_letter(self):
        #what is the letter before cursor
        cursor_possition = self.textCursor().position()
        letter_before_cursor = self.toPlainText()[cursor_possition-1]
        if letter_before_cursor in greek_dict.keys():
            #what is the greek letter assigned to letter before cursor
            greek_letter = greek_dict[letter_before_cursor]
            #replace letter before cursor by the assigned greek letter
            self.textCursor().deletePreviousChar()
            self.insertPlainText(greek_letter)
        else:
            pass

    def insert_unicode_prime_character(self):
        self.insertPlainText("ʹ") # this is U+02B9 unicode prime character that can be used for variable name