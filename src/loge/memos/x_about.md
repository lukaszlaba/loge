![alt text](x_start.png)

|Use See icon to back to your script|

#*Loge 0.3 (beta version)*

Easy and fast dynamic report generation with Python

##*License*

Loge is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Loge is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

##*The current co-lead Loge developers*

Copyright (C) 2017 - 2022 Lukasz Laba <lukaszlaba@gmail.com>

Copyright (C) 2017 Albert Defler <alde@interia.eu>

##*Other information*
Project websites:
https://loge.readthedocs.io
