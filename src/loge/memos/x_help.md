![alt text](x_start.png)

|Use See icon to back to your script|

##*What does Loge is ?*

Loge is a tool to create interactive report for scientific calculation you made with python script.

The mission of the project is to provide a simple and practical tool that will interest engineers to use Python language in their daily work.

You can use Loge to create reports ready for publication. Loge use *.py file format.
All additional Loge syntax is hidden in ''' .... ''' or behind # so the python engine does not see it, this is still python standard code file you can execute.
Loge comments is based on easy to use Markdown language syntax.

##*Let's start!*

... just run Tutorial from Help menu or open one of example file to see how does it work.

