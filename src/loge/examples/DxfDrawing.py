# -*- coding: utf-8 -*-

#! #Displaying dxf graphics from file

#! Beam static scheme - A
#%img drawing.dxf fig1

#! Beam static scheme - B
#%img drawing.dxf fig2

#! Detail - A
#%img drawing.dxf fig3

#! Frame Beam static scheme
#%img drawing.dxf fig4 450