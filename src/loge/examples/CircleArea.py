import math

#!##*Circle Area calculation*

#%img CircleArea_fig_1.png

#! For input data

r = 77 #<< - circle radius

#! from formula that everyone know 

Area = math.pi * r**2 #%requ 

#! ###So, the area of circle with var_r is val_Area.